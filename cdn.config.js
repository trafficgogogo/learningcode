const externals = {
  // 'minemap': 'minemap',
  'LIBCONFIG': 'LIBCONFIG'
}

const cdn = {
  // 开发环境
  dev: {
    css: [
      // '//minedata.cn/minemapapi/v3.2.0/minemap-dark.css',
      // 'https://minemap.minedata.cn/minemapapi/v3.4.0/minemap.css'
      // 'lib/animate.min.css'
    ],
    js: [

      'api/config.js',

    ]
  },
  // // 生产环境
  // build: {
  //   css: [
  //     // '//minedata.cn/minemapapi/v3.2.0/minemap-dark.css',
  //     // 'https://minemap.minedata.cn/minemapapi/v3.4.0/minemap.css'
  //     // 'lib/animate.min.css'
  //   ],
  //   js: [
  //     'config/api.js',
  //   ]
  // }
}

module.exports = {
  cdn,
  externals
}
