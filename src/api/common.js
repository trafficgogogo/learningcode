import request from '@/utils/request'
import LIBCONFIG from 'LIBCONFIG'
// 获取路由
export const getProvince = () => {
  return request({
    url: '/xin/province/list',
    method: 'get'
  })
}
// 通用获取下拉框
export const optionAll = () => {
  return request({
    url: LIBCONFIG.VUE_APP_BASE_APIPYTHON + 'api/option',
    method: 'get'
  })
}
