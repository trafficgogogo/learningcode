// 机构管理接口处
import request from '@/utils/request'

// 省中心列表
export const getProvinceCenterPageList = (params) => {
  return request({
    url: '/xin/dept/getProvinceCenterPageList',
    method: 'get',
    params
  })
}

// 新增省中心
export const addProvinceCenter = (data) => {
  return request({
    url: '/xin/dept/addProvinceCenter',
    method: 'post',
    data
  })
}
// 新增市中心
export const addCityCenter = (data) => {
  return request({
    url: '/xin/dept/addCityCenter',
    method: 'post',
    data
  })
}
// 新增省会员中心
export const addProvinceMember = (data) => {
  return request({
    url: '/xin/dept/addProvinceMember',
    method: 'post',
    data
  })
}
// 新增省教育局
export const addProvinceJyj = (data) => {
  return request({
    url: '/xin/dept/addProvinceJyj',
    method: 'post',
    data
  })
}
// 修改省中心部门
export const getupdatadeptId = (data) => {
  return request({
    url: '/xin/dept',
    method: 'put',
    data:data
  })
}
//  获取省中心详情
export const getdeptIdDetails = (deptId) => {
  return request({
    url: '/xin/dept/' + deptId,
    method: 'get'
  })
}
//  修改部门
export const getdeptUpdate = (data) => {
  return request({
    url: '/xin/dept',
    method: 'put',
    data:data
  })
}
//  删除部门
export const getdeptDelete = (deptId) => {
  return request({
    url: '/xin/dept/' + deptId,
    method: 'delete'
  })
}
//  激活部门
export const getdeptActive = (deptId) => {
  return request({
    url: '/xin/dept/active/' + deptId,
    method: 'put'
  })
}
//  新增省中心管理员
export const getaddProvinceCenterAdmin = (data) => {
  return request({
    url: '/xin/user/addProvinceCenterAdmin',
    method: 'post',
    data: data
  })
}
//  获取省中心列表
export const getProvinceCenterPageListvuex = (data) => {
  return request({
    url: '/xin/dept/getProvinceCenterPageList',
    method: 'get'
  })
}
//  修改用户信息
export const getuserPut = (data) => {
  return request({
    url: '/xin/user',
    method: 'put',
    data: data
  })
}
// 重置密码
export const getresetPwd = (data) => {
  return request({
    url: '/xin/user/resetPwd',
    method: 'put',
    data: data
  })
}
// 市中心列表
export const getCityCenterPageList = (params) => {
  return request({
    url: '/xin/dept/getCityCenterPageList',
    method: 'get',
    params
  })
}

// 根据省份查询市县列表
export const getCitylist = (provinceId) => {
  return request({
    url: '/xin/city/list/' + provinceId,
    method: 'get'
  })
}
// 新增市中心管理员
export const addCityCenterAdmin = (data) => {
  return request({
    url: '/xin/user/addCityCenterAdmin',
    method: 'post',
    data: data
  })
}
