// 产品管理接口处
import request from '@/utils/request'
import LIBCONFIG from 'LIBCONFIG'
// 产品列表
export const getProductList = (params) => {
  return request({
    url: LIBCONFIG.VUE_APP_BASE_APIPYTHON + 'api/product/',
    method: 'get',
    params
  })
}
// 新增产品
export const postProductList = (data) => {
  return request({
    url: LIBCONFIG.VUE_APP_BASE_APIPYTHON + 'api/product/',
    method: 'post',
    data: data
  })
}
// 获取产品
export const getProductListId = (deptId, data) => {
  return request({
    url: LIBCONFIG.VUE_APP_BASE_APIPYTHON + 'api/product/' + deptId + '/',
    method: 'get'
  })
}

// 修改产品
export const putProductListId = (deptId,data) => {
  return request({
    url: LIBCONFIG.VUE_APP_BASE_APIPYTHON + 'api/product/' + deptId + '/',
    method: 'put',
    data: data
  })
}
// 删除产品
export const deleteProductListId = (deptId) => {
  return request({
    url: LIBCONFIG.VUE_APP_BASE_APIPYTHON + 'api/product/' + deptId + '/',
    method: 'delete'
  })
}
// 产品授权列表
export const getProductresaleList = (params) => {
  return request({
    url: LIBCONFIG.VUE_APP_BASE_APIPYTHON + 'api/resale/product/',
    method: 'get',
    params
  })
}
// 新增授权产品
export const postProductresaleList = (data) => {
  return request({
    url: LIBCONFIG.VUE_APP_BASE_APIPYTHON + 'api/resale/product/',
    method: 'post',
    data: data
  })
}
// 获取授权产品
export const getProductresaleListId = (deptId, data) => {
  return request({
    url: LIBCONFIG.VUE_APP_BASE_APIPYTHON + 'api/resale/product/' + deptId + '/',
    method: 'get'
  })
}

// 修改授权产品
export const putProductresaleListId = (deptId, data) => {
  return request({
    url: LIBCONFIG.VUE_APP_BASE_APIPYTHON + 'api/resale/product/' + deptId + '/',
    method: 'put',
    data: data
  })
}
// 删除授权产品
export const deleteProductresaleListId = (deptId) => {
  return request({
    url: LIBCONFIG.VUE_APP_BASE_APIPYTHON + 'api/resale/product/' + deptId + '/',
    method: 'delete'
  })
}
// 下拉框机构 /api/option/{dept_id}/

export const optiondept_id = (deptId) => {
  return request({
    url: LIBCONFIG.VUE_APP_BASE_APIPYTHON + 'api/option/' + deptId + '/',
    method: 'get'
  })
}
