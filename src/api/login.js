import request from '@/utils/request'
import qs from 'qs'


// 登录方法
export function login(username, password, dept_id, uuid) {
  const data = {
    username,
    password,
    dept_id
  }
  return request({
    url: '/login_user/',
    method: 'post',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data:  qs.stringify(data)
  })
}
// 获取试题
export function exam(data) {
  return request({
    url: '/exam/' + data + '/',
    method: 'get'
  })
}
export function UsersfrontUsers(params) {
  return request({
    url: '/api/users/active_users/',
    method: 'get',
    params
  })
 }
// 获取学校 /api/system/active_dept/
export function getactive_deptNew() {
  return request({
    url: '/api/system/active_dept/',
    method: 'get',
  })
}
// 获取答题个人信息
export function getactive_dept(params) {
  return request({
    url: params ? '/api/plan/active/' + params : '/api/plan/active/',
    method: 'get',
  })
}
// 获取学校 http://182.44.45.169:8001/api/system/active_users/?dept_id=02aead4101224425a3464e48c74b3371
export function getactive_users(params) {
  return request({
    url: '/api/system/active_users/',
    method: 'get',
    params
  })
}
export function exampost(id,data) {
  return request({
    url: '/exam/' + id + '/',
    method: 'post',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: qs.stringify(data)
  })
}

// 注册方法
export function register(data) {
  return request({
    url: '/register',
    headers: {
      isToken: false
    },
    method: 'post',
    data: data
  })
}

// 获取用户详细信息
export function getInfo() {
  return request({
    url: '/getInfo',
    method: 'get'
  })
}

// 退出方法
export function logout() {
  return request({
    url: '/logout',
    method: 'post'
  })
}

// 获取验证码
export function getCodeImg() {
  return request({
    url: '/captchaImage',
    headers: {
      isToken: false
    },
    method: 'get',
    timeout: 20000
  })
}
